require 'dashing'
require 'sinatra/cross_origin'

configure :production do
  require 'newrelic_rpm'
end

configure do
  enable :cross_origin
  set :auth_token, 'takemehome'
  set :default_dashboard, 'index' #<==== set default dashboard like this

  helpers do
    def protected!
     # Put any authentication code you want in here.
     # This method is run before accessing any resource.
    end
  end
end

map Sinatra::Application.assets_prefix do
  run Sinatra::Application.sprockets
end

run Sinatra::Application
