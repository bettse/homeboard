require 'rest-client'
require 'json'

# Something from the previous version for when the request failed
#send_event('aww', image: "background-image:url(https://http.cat/#{response.code})")

uri = "https://www.reddit.com/r/aww.json"

SCHEDULER.every '60m', first_in: 0 do |job|
  response = RestClient.get(uri, user_agent: "ruby:dashing:1.0 (by /u/bettse)")
  posts = JSON.parse(response.body)
  urls = posts['data']['children'].map{|child| child['data']['url'] }

  # Ensure we're linking directly to an image, not a gallery etc.
  valid_urls = urls.select{|url| url.downcase.end_with?('png', 'gif', 'jpg', 'jpeg')}
  send_event('aww', image: "background-image:url(#{valid_urls.sample(1).first})")
end
