require 'json'
require 'feedjira'

itemLimit = 8

news_feeds = {
  "9to5mac" => "http://feeds.feedburner.com/9To5Mac-MacAllDay",
}

SCHEDULER.every '15m', :first_in => 0 do |job|
  news_feeds.each do |widget_id, url|
    items = Feedjira::Feed.fetch_and_parse(url).entries

    headlines = items.collect do |item|
      { label: item.title, link: item.url }
    end
    send_event(widget_id, { items: headlines.take(itemLimit) })
  end
end
