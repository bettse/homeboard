require 'yaml'
require 'forecast_io'
require 'awesome_print'
require 'ostruct'
require 'recursive-open-struct'


lat = ENV['FORECAST_LAT']
long = ENV['FORECAST_LONG']
ForecastIO.api_key = ENV['FORECAST_API_KEY']

SCHEDULER.every '10m', :first_in => 0 do |job|

  if development?
    forecast = RecursiveOpenStruct.new(JSON.parse(File.read('test/forecast.json').strip))
    forecast.minutely.data = forecast.minutely.data.collect {|m| RecursiveOpenStruct.new(m) }
  else
    begin
      forecast = ForecastIO.forecast(lat, long, {exclude: "daily,alerts,flags"}) unless development?
    rescue
      send_event('forecast', title: 'Error getting forecast')
    end
  end

  now = forecast.currently
  soon = forecast.minutely
  later = forecast.hourly
  intensity_points = []
  probability_sum = 0

  soon.data.each do |minute|
    probability_sum += minute.precipProbability
    intensity_points << { x: minute.time + 3600 * forecast.offset, y: minute.precipIntensity }
  end
  average_probability = (probability_sum / soon.data.length * 100).round(2)

  overview = {
    current_temp: "#{now.temperature.round}&deg;",
    current_icon: now.icon,
    current_desc: now.summary,
    next_icon: soon.icon,
    next_desc: soon.summary,
    later_icon: later.icon,
    later_desc: later.summary
  }

  send_event('forecast_overview', overview)
  send_event('precipitation', points: intensity_points, summaryMethod: 'highest', title: "#{average_probability}%")
end
