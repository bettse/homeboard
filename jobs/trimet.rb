require 'yaml'
require 'rest-client'
require 'awesome_print'


api_key = ENV['TRIMET_API_KEY']
stop_list = ENV['TRIMET_STOP_LIST']

SCHEDULER.every '10m', :first_in => 0 do |job|
  url = "http://developer.trimet.org/ws/V1/arrivals?json=true&locids=#{stop_list}&appid=#{api_key}"

  res = File.read('test/trimet.json').strip
  res = RestClient.get(url) unless development?
  resultSet = JSON.parse(res)['resultSet']

  stop_name = resultSet['location'].first['desc']
  arrivals = resultSet['arrival'].collect do |arrival|
    status = arrival['status'] #estimated v scheduled
    {label: arrival['shortSign'].split('-').first, timestamp: Time.parse(arrival[status]), status: status}
  end

  send_event('trimet-home', {title: stop_name, items: arrivals})
end

