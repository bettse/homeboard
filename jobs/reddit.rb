require 'rest-client'
require 'json'

class Reddit
  def initialize()
    @reddit_domain = 'https://www.reddit.com'
    # add your desired subreddits here
    @subreddits = [
      '/r/television',
      '/r/movies',
      '/r/AskReddit',
      '/r/apple',
    ]

    # the limit per subreddit to grab
    @maxcount = 5
    @maxlen = 70
  end

  def getTopPostsPerSubreddit()
    top = [];

    @subreddits.each do |subreddit, url|
      url = "#{@reddit_domain}#{subreddit}.json"
      response = RestClient.get(url, user_agent: "ruby:dashing:1.0 (by /u/bettse)")
      posts = JSON.parse(response.body)
      puts "Missing data: #{posts}" and next unless posts['data']
      puts "Missing data: #{posts['children']}" and next unless posts['data']['children']

      children = posts['data']['children']
      items = children.first(@maxcount).map do |child|
        data = child['data']
        title = data['title'].slice(0, @maxlen).gsub(/\s\w+$/, '...')

        {
          title: title,
          score: data['score'],
          comments: data['num_comments']
        }
      end
      top.push({ label: "Current top posts in #{subreddit}", items: items })
    end

   top
  end
end

@Reddit = Reddit.new();

SCHEDULER.every '2m', first_in: 0 do |job|
  posts = @Reddit.getTopPostsPerSubreddit
  send_event('reddit', { posts: posts })
end
