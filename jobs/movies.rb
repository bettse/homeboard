require 'yaml'
require 'themoviedb'
require 'awesome_print'

tmdb_key = ENV['TMDB_API_KEY']
movieLimit = (ENV['MOVIE_LIMIT'] || "6").to_i

Tmdb::Api.key(tmdb_key)

SCHEDULER.every '12h', :first_in => 0 do |job|
  now_playing = Tmdb::Movie.now_playing || []
  now_playing.sort_by!{|m| -m['popularity']}

  movies = now_playing.take(movieLimit).collect do |movie|
    {label: movie['title'], timestamp:Time.parse(movie['release_date'])}
  end

  send_event("movies", {title: 'Now Playing', items: movies})
end
