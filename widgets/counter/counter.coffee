Batman.Filters.humanizeNumber = (num) ->
  return num if isNaN(num)
  Humanize.times(num)

class Dashing.Counter extends Dashing.Widget
  ready: ->
    @count = parseInt(localStorage.getItem(@get('id')+'_count'))
    @count = 0 unless @count
    @set('count', @count)

  onData: (data) ->
    return unless @count?
    @count += 1
    @count = 0 if data.reset
    @set('count', @count)
    localStorage.setItem(@get('id')+'_count', parseInt(@count))


