# spec/spec_helper.rb or test/test_helper.rb
require 'papers'

Papers.configure do |config|
  # A whitelist of accepted licenses. Defaults to:
  #
  # [
  #   'MIT',
  #   'BSD',
  #   'Apache 2.0',
  #   'Apache-2.0',
  #   'LGPLv2.1',
  #   'LGPLv3',
  #   'Ruby',
  #   'Manually Reviewed',
  #   'Unlicensed'
  # ]
  config.license_whitelist << 'New Relic'
  config.license_whitelist << 'GPL'

  # You can specify a single license that, when used, ignores the version. Defaults to nil.
  # WARNING: You should only use this for software licensed in house.
  # config.version_whitelisted_license = 'New Relic'

  # The location of your dependency manifest. Defaults to config/papers_manifest.yml
  config.manifest_file = File.join('config', 'papers_manifest.yml')

  # Configures Papers to validate licenses for bundled gems. Defaults to true.
  config.validate_gems = true

  # Configures Papers to validate licenses for included JavaScript files. Defaults to true.
  config.validate_javascript = true

  # A list of paths where you have included JavaScript files. Defaults to:
  #
  # %w[app/assets/javascripts lib/assets/javascripts vendor/assets/javascripts]
  config.javascript_paths << File.join('public', 'javascripts')

  # Configures Papers to validate licenses for bower components. Defaults to false.
  config.validate_bower_components = false

  # Configures where Papers should look for bower components. Each component
  # must have a .bower.json file in its directory for Papers to see it.
  # config.bower_components_path = 'vendor/assets/components'
end
