# Using RSpec
require_relative '../spec_helper'

describe 'Papers License Validation' do
  subject(:validator) { Papers::LicenseValidator.new }

  it 'knows and is satisfied by all dependency licenses' do
    expect(validator).to be_valid, -> { "License validation failed:\n#{validator.errors.join("\n")}" }
  end
end

